# Code Exercise

## Running.

This is standard Spring Boot maven application. I developed and tested it using Java 8.

To run this from the repo you can type

    `mvn spring-boot:run`
 
Alternatively it can be built with

    `mvn clean install`
    
This will create a jar file which can be run using

    `java -jar target/mhexercise-0.0.1-SNAPSHOT.jar`
     
The application runs an embedded server on port 8080.

## Rest Calls

* localhost:8080/users - will return a json list of the usernames in the system.
* localhost:8080/user/bob - will return a json list of the tasks for user 'bob'.
* localhost:8080/user/bob/2016-09-09 - will return a json lists of bob's tasks for a specific date. 

## Coding Notes

* The data from data.csv is loaded into TaskDataStore by DataStoreLoader. This is done in the init() method in 
MhexerciseApplication.

* The primary key for User is "username". From the requirements username is guaranteed to be unique. The DataStoreLoader
 assigns a unique Integer id to each task.
 
* The pojos (User, Task) are immutable. The requirements are to load and retrieve this data. I believe this 
is the correct decision.
 
* I started to write code to allow you to specify a datafile from the command line. This is not working yet. I may 
come back and fix this. If you want to change the data file, it is in src/main/resources/data.csv by default.

* There are unit tests for TaskDataStore and DataStoreLoader.
