package com.brown.mhexercise.datastore;

import com.brown.mhexercise.pojos.Task;
import com.brown.mhexercise.pojos.User;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
/**
 * Stores User/Task data and provides methods to retrieve.
 */
public class TaskDataStore {

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    // Where the data is stored, keyed for easy access by username;
    private Map<String, User> usermap = new HashMap<>();




    public void setUserData(List<User> users) {

        this.usermap.clear();

        // This should check for duplicates?
        for (User user : users) {
            usermap.put(user.getUsername(), user);
        }
    }

    /**
     * @param username
     * @return the User for username
     */
    public User getUser(String username) {
        return usermap.get(username);

    }
    /**
     * Get a list of users
     */
    public List<String> getUsernames() {
      List<String> names = new ArrayList<>(usermap.keySet());
      Collections.sort(names);
      return names;
    }

    /**
     * Get Tasks for user
     */
    public List<Task> getTasksForUser (String username) {
        User user = usermap.get(username);

        if (user == null) {
            return Collections.emptyList();
        }

        return user.getTasks();
    }

    /**
     * Get Tasks for user and date
     */
    public List<Task> getTasksForUserDate(String username, LocalDate date) {
        return getTasksForUser(username).stream().
                filter(d -> d.getDate().equals(date)).
                collect(Collectors.toList());
    }
}
