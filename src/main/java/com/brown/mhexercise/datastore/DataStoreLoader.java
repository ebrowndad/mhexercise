package com.brown.mhexercise.datastore;

import com.brown.mhexercise.pojos.Task;
import com.brown.mhexercise.pojos.User;
import org.apache.commons.lang.mutable.MutableInt;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.*;
import java.text.ParseException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.*;

public class DataStoreLoader {

    Log LOG = LogFactory.getLog(DataStoreLoader.class);

    private final TaskDataStore dataStore;

    public DataStoreLoader (TaskDataStore dataStore) {
        this.dataStore = dataStore;
    }

    public void loadFromInputStream(InputStream input) throws FileNotFoundException {

        Map<String, List<Task>> userMap = new HashMap<>();

        final MutableInt taskNumber = new MutableInt(0);

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(input))) {

            reader.readLine(); // get rid of first (header) line
            reader.lines().forEach(l -> {
                String[] parts = l.split(",");

                if (parts.length != 3) {
                    LOG.error("Invalid Line" + l);
                    return;
                }

                String username = parts[0];
                String description = parts[1];
                String datestring = parts[2];

                try {
                    LocalDate date = LocalDate.parse(datestring);
                    Task task = new Task(taskNumber.intValue(), description, date);
                    taskNumber.increment();

                    if (userMap.containsKey(username)) {
                        userMap.get(username).add(task);
                    } else {
                        List<Task> tlist = new ArrayList<>();
                        tlist.add(task);
                        userMap.put(username, tlist);
                    }

                } catch (DateTimeException e) {
                    LOG.error("invalid datestring " + datestring);
                    return;
                }

            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<User> users = new ArrayList<>();

        for (String username : userMap.keySet()) {
            users.add(new User(username, userMap.get(username)));
        }

        dataStore.setUserData(users);
    }
}
