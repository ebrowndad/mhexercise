package com.brown.mhexercise;

import com.brown.mhexercise.datastore.DataStoreLoader;
import com.brown.mhexercise.datastore.TaskDataStore;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ClassPathResource;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@SpringBootApplication
public class MhexerciseApplication {

	Log log = LogFactory.getLog(MhexerciseApplication.class);

	@Autowired
	public TaskDataStore dataStore;

	@Value("${datafile:}")
	String dataFilePath;

	@PostConstruct
	private void init() {
		log.info("Initializing Coding Exercise");

		InputStream instream = null;

		try {

			if (dataFilePath.isEmpty()) {
				instream = new ClassPathResource("data.csv").getInputStream();
			} else {
				log.info("Loading data from " + dataFilePath);
				instream = new FileInputStream(new File(dataFilePath));
			}

			DataStoreLoader loader = new DataStoreLoader(dataStore);
			loader.loadFromInputStream(instream);
		} catch (IOException e) {
			log.error("Unable to load datastore " + dataStore, e);
		} finally {

			// this should be a try with resources block, but I am adding the ability to call from command line quickly
			if (instream != null) {
				try {
					instream.close();
				} catch (IOException e) {
				}
			}
		}

		// ...
	}

	public static void main(String[] args) {
		SpringApplication.run(MhexerciseApplication.class, args);
	}

}
