package com.brown.mhexercise.pojos;


import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;
import java.util.Date;

/**
 * Represents a Task.
 *
 * This is intentionally immutable (see README for rationale).
 *
 */

public class Task {

    // This is the primary key. A user can have duplicate tasks with the same date or description.
    private Integer id;

    private String description;

    private LocalDate date;

    /**
     * Construct a Task
     * @param id
     * @param description
     * @param date
     */
    public Task(Integer id, String description, LocalDate date) {
        this.id = id;
        this.description = description;
        this.date = date;
    }

    /**
     * @return the Tasks id
     */
    public Integer getId() {
        return id;
    }

    /**
     * The Tasks description
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return the due date of the task.
     */
    public LocalDate getDate() {
        return date;
    }
}
