package com.brown.mhexercise.pojos;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Represents a TODO List user.
 *
 * This is intentionally immutable (see README for rationale).
 *
 */
public class User {

    // This is assumed unique and is the "primary key".
    private String username;

    // Tasks assigned to users (sort by date).
    private List<Task> tasks;

    /**
     * Constructs a User with tasks sorted by Date
     *
     * @param username
     * @param tasks
     */
    public User(String username, List<Task> tasks) {
        this.username = username;
        this.tasks = new ArrayList<>(tasks);

        // sort by date
        this.tasks.sort(Comparator.comparing(Task::getDate));
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @return a list of tasks for user.
     */
    public List<Task> getTasks() {
        return tasks;
    }
}
