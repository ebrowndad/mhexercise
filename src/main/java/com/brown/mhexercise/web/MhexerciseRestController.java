package com.brown.mhexercise.web;

import com.brown.mhexercise.datastore.TaskDataStore;
import com.brown.mhexercise.pojos.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.List;

@RestController
public class MhexerciseRestController {

    @Autowired
    public TaskDataStore dataStore;

    @RequestMapping("/users")
    public List<String> getUsers() {
        return dataStore.getUsernames();
    }

    @RequestMapping("/user/{username}")
    public List<Task> getTasksForUser(@PathVariable String username) {
        return dataStore.getTasksForUser(username);
    }

    @RequestMapping("/user/{username}/{datestring}")
    public List<Task> getTasksForUserDate(@PathVariable String username, @PathVariable String datestring) {
        LocalDate date = null;

        try {
            date = LocalDate.parse(datestring);
        } catch (DateTimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid date " + datestring);
        }

        return dataStore.getTasksForUserDate(username, date);
    }

}
