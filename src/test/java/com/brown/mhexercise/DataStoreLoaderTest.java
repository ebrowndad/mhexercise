package com.brown.mhexercise;

import com.brown.mhexercise.datastore.DataStoreLoader;
import com.brown.mhexercise.datastore.TaskDataStore;
import com.brown.mhexercise.pojos.User;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;

public class DataStoreLoaderTest {

    @Test
    public void loadDataTest() throws IOException {
        InputStream data = new ClassPathResource("data.csv").getInputStream();
        TaskDataStore dataStore = new TaskDataStore();
        DataStoreLoader loader = new DataStoreLoader(dataStore);

        loader.loadFromInputStream(data);

        User bob =  dataStore.getUser("bob");
        assertEquals(2, bob.getTasks().size());

        assertEquals ("2016-09-09", bob.getTasks().get(0).getDate().toString());
    }
}
