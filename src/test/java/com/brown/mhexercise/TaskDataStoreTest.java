package com.brown.mhexercise;

import com.brown.mhexercise.datastore.TaskDataStore;
import com.brown.mhexercise.pojos.Task;
import com.brown.mhexercise.pojos.User;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class TaskDataStoreTest {

    private TaskDataStore underTest;

    @Before
    public void setup() throws Exception {
        Task t1 = new Task(1, "Do something", LocalDate.parse("2019-02-01"));
        Task t2 = new Task(2, "Do something else", LocalDate.parse("2019-01-01"));
        Task t3 = new Task(3, "Do this other thing", LocalDate.parse("2019-02-04"));
        Task t4 = new Task(4, "Do something new", LocalDate.parse("2019-02-06"));

        User alice = new User("alice", Arrays.asList(t1, t2));
        User bob = new User("bob", Arrays.asList(t3, t4));

        this.underTest = new TaskDataStore();
        underTest.setUserData(Arrays.asList(alice, bob));
    }

    @Test
    public void sortedTasks() {
        User alice = underTest.getUser("alice");
        List<Task> atasks = alice.getTasks();

        // sorted by date
        assertEquals((Integer)2, atasks.get(0).getId());
        assertEquals((Integer)1, atasks.get(1).getId());

    }

    @Test
    public void getUserNames() {
        List<String> names = underTest.getUsernames();

        assertEquals("alice", names.get(0));
        assertEquals( "bob", names.get(1));

    }

    @Test
    public void getTasksForUser() {
        List<Task> tasks = underTest.getTasksForUser("bob");

        assertEquals ("Do something new", tasks.get(1).getDescription());
    }

    @Test
    public void getTasksForNonexistantUser() throws Exception {
        List<Task> tasks = underTest.getTasksForUser("fred");
        List<Task> tasks2 = underTest.getTasksForUserDate("fred", LocalDate.parse("2019-02-28"));


        assertTrue(tasks.isEmpty());
        assertTrue(tasks2.isEmpty());


    }

    @Test
    public void getTasksByDate() throws Exception {
        List<Task> tasks = underTest.getTasksForUserDate("bob", LocalDate.parse("2019-02-04"));

        assertEquals("Do this other thing", tasks.get(0).getDescription());
        assertEquals(1, tasks.size());

    }

    @Test
    public void getTasksByNonexistantDate() throws Exception {
        List<Task> tasks = underTest.getTasksForUserDate("bob", LocalDate.parse("2019-02-27"));

        assertEquals(0, tasks.size());

    }



}
